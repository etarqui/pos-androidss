package com.sietecumbres.pos;

public class SellItem {

	// private variables
	public int _id;
	public String _product;
	public int _quantity;
	public int _weight;
	public int _value;
	public int _sell_id;
	public int _product_n;

	public SellItem() {
	}

	// constructor
	public SellItem(int id, String product, int quantity, int weight, int value, int sell_id) {
		this._id = id;
		this._product = product;
		this._quantity = quantity;
		this._weight = weight;
		this._value = value;
		this._sell_id = sell_id;

	}

	// constructor
	public SellItem(String product, int quantity, int weight, int value, int sell_id) {
		this._product = product;
		this._quantity = quantity;
		this._weight = weight;
		this._value = value;
		this._sell_id = sell_id;
	}

	// getting ID
	public int getID() {
		return this._id;
	}

	// setting id
	public void setID(int id) {
		this._id = id;
	}

	// getting name
	public String getProduct() {
		return this._product;
	}

	// setting name
	public void setProduct(String product) {
		this._product = product;
	}

	// getting name
	public int getQuantity() {
		return this._quantity;
	}

	// setting name
	public void setQuantity(int quantity) {
		this._quantity = quantity;
	}

	// getting phone number
	public int getWeight() {
		return this._weight;
	}

	// setting phone number
	public void setWeight(int weight) {
		this._weight = weight;
	}

	// getting phone number
	public int getValue() {
		return this._value;
	}

	// setting phone number
	public void setValue(int value) {
		this._value = value;
	}

	// getting phone number
	public int getSellId() {
		return this._sell_id;
	}

	// setting phone number
	public void setSellId(int sell_id) {
		this._sell_id = sell_id;
	}

	// getting phone number
	public int getProductN() {
		return this._product_n;
	}

	// setting phone number
	public void setProductN(int product_n) {
		this._product_n = product_n;
	}

}