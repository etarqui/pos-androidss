package com.sietecumbres.pos;

public class ModelUnits {

	private int productUnits;

	public ModelUnits(int productUnits){
		this.productUnits = productUnits;
	}

	public int getProductUnits() {
		return productUnits;
	}

	public void setProductUnits(int Units) {
		this.productUnits = Units;
	}
}
