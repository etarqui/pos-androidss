package com.sietecumbres.pos;

public class ModelProducts {

	private String productId;
	private String productName;
	private String productImage;
	private String productFixedPrice;
	private int productPrice;

	public ModelProducts(String productId, String productName, String productImage, String productFixedPrice, int productPrice)
	{
		this.productId  = productId;
		this.productName  = productName;
		this.productImage  = productImage;
		this.productFixedPrice  = productFixedPrice;
		this.productPrice = productPrice;
	}

	public String getProductId() {

		return productId;
	}

	public String getProductName() {

		return productName;
	}

	public String getProductImage() {

		return productImage;
	}

	public String getProductFixedPrice() {

		return productFixedPrice;
	}

	public int getProductPrice() {

		return productPrice;
	}

}