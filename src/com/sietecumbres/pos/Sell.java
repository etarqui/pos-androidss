package com.sietecumbres.pos;

public class Sell {

	// private variables
	public int _id;
	public int _total_sell;
	public int _total_weight;

	public Sell() {
	}

	// constructor
	public Sell(int id, int total_sell, int total_weight) {
		this._id = id;
		this._total_sell = total_sell;
		this._total_weight = total_weight;

	}

	// constructor
	public Sell(int total_sell, int total_weight) {
		this._total_sell = total_sell;
		this._total_weight = total_weight;
	}

	// getting ID
	public int getID() {
		return this._id;
	}

	// setting id
	public void setID(int id) {
		this._id = id;
	}

	// getting name
	public int getTotalSell() {
		return this._total_sell;
	}

	// setting name
	public void setTotalSell(int total_sell) {
		this._total_sell = total_sell;
	}

	// getting phone number
	public int getTotalWeight() {
		return this._total_weight;
	}

	// setting phone number
	public void setTotalWeight(int total_weight) {
		this._total_weight = total_weight;
	}

}