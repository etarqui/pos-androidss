package com.sietecumbres.pos;

import java.util.ArrayList;
import java.util.Collection;

public class ModelCart{

	private ArrayList<ModelProducts> cartProducts = new ArrayList<ModelProducts>();
	private ArrayList<ModelUnits> cartUnits = new ArrayList<ModelUnits>();
	
	public ModelProducts getProducts(int pPosition) {
		return cartProducts.get(pPosition);
	}

	public void setProducts(ModelProducts Products) {
		cartProducts.add(Products);
	}
	
	public ModelUnits getUnits(int pPosition) {
		return cartUnits.get(pPosition);
	}

	public void setUnits(ModelUnits Units) {
		cartUnits.add(Units);
	}

	public int getCartSize() {
		return cartProducts.size();
	}

	public boolean checkProductInCart(ModelProducts aProduct) {
		return cartProducts.contains(aProduct);
	}
	
	public void emptyCart(){
		cartProducts.clear();
		cartUnits.clear();
	}

}