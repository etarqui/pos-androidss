package com.sietecumbres.pos;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class NetworkChangeReceiver extends BroadcastReceiver {
	
	@Override
	public void onReceive(final Context context, final Intent intent) {

		String status = NetworkUtil.getConnectivityStatusString(context);
		Toast.makeText(context, status, Toast.LENGTH_LONG).show();
		int intStatus = NetworkUtil.getConnectivityStatus(context);

		if(intStatus == 1 || intStatus == 2){
			//sendSells(context);
			Intent i = new Intent();
	        i.setClassName("com.sietecumbres.pos", "com.sietecumbres.pos.POSActivity");
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(i);
		}
	}
}