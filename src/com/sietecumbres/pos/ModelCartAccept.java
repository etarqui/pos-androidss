package com.sietecumbres.pos;

import java.util.ArrayList;
import java.util.Collection;

public class ModelCartAccept{

	private ArrayList<ModelProductsAccept> cartProducts = new ArrayList<ModelProductsAccept>();
	private ArrayList<ModelUnitsAccept> cartUnits = new ArrayList<ModelUnitsAccept>();
	
	public ModelProductsAccept getProducts(int pPosition) {
		return cartProducts.get(pPosition);
	}

	public void setProducts(ModelProductsAccept Products) {
		cartProducts.add(Products);
	}
	
	public ModelUnitsAccept getUnits(int pPosition) {
		return cartUnits.get(pPosition);
	}

	public void setUnits(ModelUnitsAccept Units) {
		cartUnits.add(Units);
	}

	public int getCartSize() {
		return cartProducts.size();
	}

	public boolean checkProductInCart(ModelProductsAccept aProduct) {
		return cartProducts.contains(aProduct);
	}
	
	public void emptyCart(){
		cartProducts.clear();
		cartUnits.clear();
	}

}