package com.sietecumbres.pos;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.savagelook.android.UrlJsonAsyncTask;

import android.os.Bundle;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class RequestActivity extends Activity {
	private final static String PRODUCTS_URL = Global.getInstance().getDevelopment() + "/api/v1/products.json";
	private final static String REQUEST_URL = Global.getInstance().getDevelopment() + "/api/v1/request.json";
	private SharedPreferences mPreferences;
	
	Controller aController = null;
	ModelProducts productObject = null;
	ModelUnits unitObject = null;
	
	Button accept = null;
	
	Integer status = 0, requestFromBD = 1;
	Button goBack;
	TextView productName, productPrice, productPriceText;
	
	Typeface tf;
	
	DatabaseHandler dbHandler = new DatabaseHandler(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_request);
		
		tf = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueDeskUI.ttf");

		goBack = (Button) findViewById(R.id.goBack);
		goBack.setTypeface(tf);
		
		mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
		
		//Get Global Controller Class object (see application tag in AndroidManifest.xml)
        aController = (Controller) getApplicationContext();
        
        // Ask for request in BD
		dbHandler.open();
		
		Cursor cur = dbHandler.countRequests();
		if (cur != null) {
		    cur.moveToFirst();
		    if(cur.getInt(0) > 0 && (NetworkUtil.getConnectivityStatus(this) == 1 || NetworkUtil.getConnectivityStatus(this) == 2)){
		    	sendRequests();
		    }
		}
        
        // Set buttons
        accept = (Button)findViewById(R.id.acceptRequest);
        //Create click listener for dynamically created button
        accept.setOnClickListener(new OnClickListener() {
            
            public void onClick(View v) {
            	
            	for(int j = 0; j < Global.getInstance().getProductsCount(); j++){
                    //Clicked button index

                    EditText units = (EditText)findViewById(j+1);
                    
                    if(units.getText().length() != 0){
                    	
                    	unitObject = new ModelUnits(
                    			Integer.parseInt(units.getText().toString())
                        );
                    	
                        // Get product instance for index
                        ModelProducts tempProductObject = aController.getProducts(j);
                        
                        //Check Product already exist in Cart or Not
                        if(!aController.getCart().checkProductInCart(tempProductObject))
                        {    
                            // Product not Exist in cart so add product to
                            // Cart product arraylist
                            aController.getCart().setProducts(tempProductObject);
                            aController.getCart().setUnits(unitObject);
                        }else{
                            // Cart product arraylist contains Product 
                            Toast.makeText(getApplicationContext(), 
                                    "Este producto "+(j+1)+" ya est� en el carrito.", 
                                    Toast.LENGTH_LONG).show();
                        }   
                    }
            	}
            	
            	requestSummaryDialog();
            	
            }
        });
	}
	
	@Override
	public void onResume() {
	    super.onResume();

	    if (mPreferences.contains("AuthToken")) {
	    	showProducts(PRODUCTS_URL);
	    } else {
	        Intent intent = new Intent(RequestActivity.this, WelcomeActivity.class);
	        startActivityForResult(intent, 0);
	    }
	}
	
	private void showProducts(String url) {
		if(Global.getInstance().hasConnection(RequestActivity.this)){
			GetProducts getProducts = new GetProducts(RequestActivity.this);
			getProducts.setMessageLoading("Cargando...");
			getProducts.execute(url + "?authentication_token=" + mPreferences.getString("AuthToken", ""));
		}else{
			Toast.makeText(RequestActivity.this, "El dispositivo no tiene conexi�n a Internet.", Toast.LENGTH_LONG).show();
		}
	}
	
	private class GetProducts extends UrlJsonAsyncTask {
	    public GetProducts(Context context) {
	        super(context);
	    }

		@Override
        protected void onPostExecute(JSONObject json) {
            try {
            	
                JSONArray jsonProducts = json.getJSONArray("products");
                int size = jsonProducts.length();
                
                TableLayout productsLayout = (TableLayout) findViewById(R.id.requestTable);
                
                for(int n = 1, s = productsLayout.getChildCount(); n < s; ++n) {
                	TableRow row = (TableRow)productsLayout.getChildAt(n);
                	row.setVisibility(View.GONE);
                }
                
                int loader = R.drawable.ic_launcher;
                ImageLoader imgLoader = new ImageLoader(getApplicationContext());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                //params.gravity = Gravity.CENTER_HORIZONTAL;
                //params.gravity = Gravity.CENTER_VERTICAL;
                //params.setMargins(R.dimen.units_button, 0, 0, 0);
                
                for(int i = 0; i < size; ++i){
                	
                	/*
                	 * Guardar los productos en el modelo
                	 */
                    productObject = new ModelProducts(
                    		jsonProducts.getJSONObject(i).getString("id"),
                    		jsonProducts.getJSONObject(i).getString("name"),
                    		jsonProducts.getJSONObject(i).getString("image"),
                    		jsonProducts.getJSONObject(i).getString("fixed_price"),
                    		jsonProducts.getJSONObject(i).getInt("price")
                    );

                    aController.setProducts(productObject);
                    
                    Global.getInstance().setProductsCount(i+1);
                    
                    /*
                     * Mostrar los productos en la vista
                     */
                	TableRow row = (TableRow) getLayoutInflater().inflate(R.layout.product_row, null);
                	
                	if(jsonProducts.getJSONObject(i).getString("image") != null){
                        String image = jsonProducts.getJSONObject(i).getString("image");
                    	ImageView product = (ImageView)row.findViewById(R.id.productImage);
                        imgLoader.DisplayImage(image, loader, product);
                	}
                	
            		((TextView)row.findViewById(R.id.productName)).setText(jsonProducts.getJSONObject(i).getString("name"));
            		((TextView)row.findViewById(R.id.productPrice)).setText(jsonProducts.getJSONObject(i).getString("fixed_price"));
            		((TextView)row.findViewById(R.id.productPriceText)).setTypeface(tf);
            		((TextView)row.findViewById(R.id.productName)).setTypeface(tf);
            		((TextView)row.findViewById(R.id.productPrice)).setTypeface(tf);
            		
            		LinearLayout ll = (LinearLayout)row.findViewById(R.id.unitsRow);
            		final EditText et = new EditText(RequestActivity.this);
            		et.setId(i+1);
            		et.setLayoutParams(params);
            		et.setInputType(InputType.TYPE_CLASS_NUMBER);
            		et.setBackgroundResource(R.drawable.bg_action_buttons);
            		et.setHint(R.string.units);
            		et.setTextColor(0xffffffff);
            		et.setHintTextColor(0xffffffff);
            		et.setWidth(300);
            		et.setPadding(70, 0, 0, 0);
            		
            		ll.addView(et);
            		
            		productsLayout.addView(row);
            		productsLayout.requestLayout();
                }
                
            } catch (Exception e) {
            	Toast.makeText(context, "Ocurri� un error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            } finally {
            	super.onPostExecute(json);
            }
	    }
	}
	
	public void requestSummaryDialog() {
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.request_summary_dialog);
		
		TableLayout summaryLayout = (TableLayout) dialog.findViewById(R.id.requestSummaryTable);

        int loader = R.drawable.ic_launcher;
        ImageLoader imgLoader = new ImageLoader(getApplicationContext());
        
        // Get Cart Size
        final int cartSize = aController.getCart().getCartSize();
        int requestTotal = 0;
        int pTotal = 0;
        
        if(cartSize >0){
            for(int i=0;i<cartSize;i++){
                //Get product details
                
                int pPrice = aController.getCart().getProducts(i).getProductPrice();
                int pUnits = aController.getCart().getUnits(i).getProductUnits();
                
                pTotal = pPrice * pUnits;
                requestTotal += pTotal;
                
        		TableRow row = (TableRow) getLayoutInflater().inflate(R.layout.request_summary_row, null);
        		((TextView)row.findViewById(R.id.unitsSummary)).setText(pUnits+"");
        		((TextView)row.findViewById(R.id.valueSummary)).setText("$"+pTotal);
        		((TextView)row.findViewById(R.id.unitsSummary)).setTypeface(tf);
        		((TextView)row.findViewById(R.id.valueSummary)).setTypeface(tf);
        		ImageView thumb = (ImageView)row.findViewById(R.id.imageSummary);
                String url = aController.getCart().getProducts(i).getProductImage();
                imgLoader.DisplayImage(url, loader, thumb);
        		summaryLayout.addView(row);
        		summaryLayout.requestLayout();
            }
        }else{
        	Toast.makeText(RequestActivity.this, "El carrito est� vac�o.", Toast.LENGTH_LONG).show();
        }
        
        Global.getInstance().setRequestTotal(requestTotal);
        TableRow rowTotal = (TableRow) getLayoutInflater().inflate(R.layout.total_request_summary_row, null);
        ((TextView)rowTotal.findViewById(R.id.totalSummary)).setText("$"+requestTotal);
        ((TextView)rowTotal.findViewById(R.id.totalSummary)).setTypeface(tf);
        summaryLayout.addView(rowTotal);
		summaryLayout.requestLayout();
		
		Button acceptButton = (Button) dialog.findViewById(R.id.accept);
		acceptButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				acceptRequest();
				dialog.dismiss();
			}
		});
		
		Button cancelButton = (Button) dialog.findViewById(R.id.cancel);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				emptyCart();
				dialog.dismiss();
			}
		});
		
		dialog.show();
	}
	
	public void acceptRequest() {
		if(Global.getInstance().hasConnection(RequestActivity.this)){

			if(dbHandler.countRequestsInt() == 0){
				RequestTask requestTask = new RequestTask(RequestActivity.this);
				requestTask.setMessageLoading("Enviando...");
				requestTask.execute(REQUEST_URL + "?authentication_token=" + mPreferences.getString("AuthToken", ""));
			}else{
				RequestTaskDB requestTaskDB = new RequestTaskDB(RequestActivity.this);
				requestTaskDB.setMessageLoading("Enviando...");
				requestTaskDB.execute(REQUEST_URL + "?authentication_token=" + mPreferences.getString("AuthToken", ""));
			}
    	}else{
    		Toast.makeText(RequestActivity.this, "El dispositivo no tiene conexi�n a Internet, el pedido ser� almacenado en Base de Datos.", Toast.LENGTH_LONG).show();
    		requestOffline();
    	}
	}
	
	public void requestOffline() {

		dbHandler.open();
		Integer request_id = 1;
        
		dbHandler.insertRequest(Global.getInstance().getRequestTotal());
		
		request_id = dbHandler.getLastRequestId();
		
        // Get Cart Size
        final int cartSize = aController.getCart().getCartSize();
        int pTotal = 0;
        
        if(cartSize >0){
            for(int i=0;i<cartSize;i++){
            	
                int pPrice = aController.getCart().getProducts(i).getProductPrice();
                int pUnits = aController.getCart().getUnits(i).getProductUnits();
                
                pTotal = pPrice * pUnits;
                
            	dbHandler.insertRequestItem(
            			aController.getCart().getProducts(i).getProductId(),
            			pUnits,
            			pTotal,
            			request_id
            	);
        		
            }
        }else{
        	Toast.makeText(RequestActivity.this, "El carrito est� vac�o.", Toast.LENGTH_LONG).show();
        }
        
        dbHandler.close();
        Intent intent = new Intent(getApplicationContext(), RequestActivity.class);
        startActivity(intent);
        finish();
	}
	
	private class RequestTask extends UrlJsonAsyncTask {
	    public RequestTask(Context context) {
	        super(context);
	    }

	    @Override
	    protected JSONObject doInBackground(String... urls) {
	        DefaultHttpClient client = new DefaultHttpClient();
	        HttpPost post = new HttpPost(urls[0]);
	        JSONObject holder = new JSONObject();
	        JSONObject total = new JSONObject();
	        String response = null;
	        JSONObject json = new JSONObject();

	        try {
	            try {
	                json.put("success", false);
	                json.put("info", "El dispositivo no tiene conexi�n a Internet.");
	                
	                // Get Cart Size
	                final int cartSize = aController.getCart().getCartSize();
	                int requestTotal = 0;
	                int pTotal = 0;
	                List  l1 = new LinkedList();
	                
	                if(cartSize >0){
	                    for(int i=0;i<cartSize;i++){

	            	        JSONObject request = new JSONObject();
	                    	
	                        //Get product details
	                    	String id = aController.getCart().getProducts(i).getProductId();
	                        int pPrice = aController.getCart().getProducts(i).getProductPrice();
	                        int pUnits = aController.getCart().getUnits(i).getProductUnits();
	                        
	                        pTotal = pPrice * pUnits;
	                        requestTotal += pTotal;
	                        
	                        request.put("_id", id);
	                        request.put("quantity", pUnits);
	                        request.put("total_price", pTotal);
	                        l1.add(request);
	                    }
	                }else{
	                	Toast.makeText(RequestActivity.this, "El carrito est� vac�o.", Toast.LENGTH_LONG).show();
	                	response = null;
	                }
	                
	                holder.put("request", l1);
	                
	                Global.getInstance().setRequesIdFromAPI(Global.getInstance().getRequesIdFromAPI() + 1);
	                
	                total.put("total_price", requestTotal);
	                total.put("request_id", Global.getInstance().getRequesIdFromAPI() + 1);
	                holder.put("total", total);
	            	
	                StringEntity se = new StringEntity(holder.toString());
	                post.setEntity(se);
	                
	                // setup the request headers
	                post.setHeader("Accept", "application/json");
	                post.setHeader("Content-Type", "application/json");

	                ResponseHandler<String> responseHandler = new BasicResponseHandler();
	                
	        		if(Global.getInstance().hasConnection(RequestActivity.this))
	        			response = client.execute(post, responseHandler);
	        		else
	        			response = null;
	        		
	                json = new JSONObject(response);
	                
	            } catch (HttpResponseException e) {
	                e.printStackTrace();
	                Log.e("ClientProtocol", "" + e);
	                json.put("info", "Email o Contrase�a incorrectos.");
	            } catch (IOException e) {
	                e.printStackTrace();
	                Log.e("IO", "" + e);
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();
	            Log.e("JSON", "" + e);
	        }

	        return json;
	    }

	    @Override
	    protected void onPostExecute(JSONObject json) {
	        try {
	            if (json.getBoolean("success")) {

	            	emptyCart();

	                Intent intent = new Intent(getApplicationContext(), POSActivity.class);
	                startActivity(intent);
	                finish();
	            }
	            Toast.makeText(context, json.getString("info"), Toast.LENGTH_LONG).show();
	        } catch (Exception e) {
	            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
	        } finally {
	            super.onPostExecute(json);
	        }
	    }
	}
	
	private class RequestTaskDB extends UrlJsonAsyncTask {
	    public RequestTaskDB(Context context) {
	        super(context);
	    }

	    @Override
	    protected JSONObject doInBackground(String... urls) {
	        DefaultHttpClient client = new DefaultHttpClient();
	        HttpPost post = new HttpPost(urls[0]);
	        JSONObject holder = new JSONObject();
	        JSONObject total = new JSONObject();
	        String response = null;
	        JSONObject json = new JSONObject();

	        try {
	            try {
	                json.put("success", false);
	                json.put("info", "El dispositivo no tiene conexi�n a Internet.");
	                
	                int requestTotal = 0;
	                int pTotal = 0;
	                List  l1 = new LinkedList();

	        		ArrayList<RequestItem> request_items_array = dbHandler.Get_Request_Items(requestFromBD);

	                for (int j = 0; j < request_items_array.size(); j++) {
	                	
            	        JSONObject request = new JSONObject();
                    	
                        //Get product details
                    	String id = request_items_array.get(j).getProduct();
                        int pPrice = request_items_array.get(j).getValue();
                        int pUnits = request_items_array.get(j).getQuantity();
                        
                        pTotal = pPrice * pUnits;
                        requestTotal += pTotal;
                        
                        request.put("_id", id);
                        request.put("quantity", pUnits);
                        request.put("total_price", pTotal);
                        l1.add(request);
	                }
	                
	                holder.put("request", l1);
	                
	                Global.getInstance().setRequesIdFromAPI(Global.getInstance().getRequesIdFromAPI() + 1);
	                
	                total.put("total_price", requestTotal);
	                total.put("request_id", requestFromBD);
	                holder.put("total", total);
	            	
	                StringEntity se = new StringEntity(holder.toString());
	                post.setEntity(se);
	                
	                // setup the request headers
	                post.setHeader("Accept", "application/json");
	                post.setHeader("Content-Type", "application/json");

	                ResponseHandler<String> responseHandler = new BasicResponseHandler();
	                
	        		if(Global.getInstance().hasConnection(RequestActivity.this))
	        			response = client.execute(post, responseHandler);
	        		else
	        			response = null;
	        		
	                json = new JSONObject(response);
	                
	            } catch (HttpResponseException e) {
	                e.printStackTrace();
	                Log.e("ClientProtocol", "" + e);
	                json.put("info", "Email o Contrase�a incorrectos.");
	            } catch (IOException e) {
	                e.printStackTrace();
	                Log.e("IO", "" + e);
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();
	            Log.e("JSON", "" + e);
	        }

	        return json;
	    }

	    @Override
	    protected void onPostExecute(JSONObject json) {
	        try {
	            if (json.getBoolean("success")) {
	            	dbHandler.open();
	            	dbHandler.removeRequest(requestFromBD);
	            	dbHandler.removeRequestItem(requestFromBD);
	            	
	            	emptyCart();

	                Intent intent = new Intent(getApplicationContext(), POSActivity.class);
	                startActivity(intent);
	                finish();
	            }
	            Toast.makeText(context, json.getString("info"), Toast.LENGTH_LONG).show();
	        } catch (Exception e) {
	            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
	        } finally {
	            super.onPostExecute(json);
	        }
	    }
	}
	
	public void cancelRequest(View view) {
		emptyCart();
	}
	
	public void sendRequests() {

		ArrayList<Request> requests_array = dbHandler.Get_Requests();

        for (int i = 0; i < requests_array.size(); i++) {
            int tidno = requests_array.get(i).getID();
            int total_request = requests_array.get(i).getTotalPrice();
            
            Global.getInstance().setRequestTotal(total_request);
            
            requestFromBD = tidno;
            acceptRequest();
        }
        dbHandler.close();
	}
	
	public void emptyCart() {
		emptyEditTexts();
    	aController.getCart().emptyCart();
    	Global.getInstance().setRequestTotal(0);
	}
	
	public void emptyEditTexts() {
		final int size = Global.getInstance().getProductsCount();
		for(int i = 1; i <= size; i++){
			EditText et = (EditText)findViewById(i);
			et.setText("");
		}
	}
	
	public void goBack(View view){
        Intent intent = new Intent(getApplicationContext(), POSActivity.class);
        startActivity(intent);
        finish();
	}

}
