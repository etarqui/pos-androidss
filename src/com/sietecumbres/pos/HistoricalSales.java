package com.sietecumbres.pos;

import org.json.JSONArray;
import org.json.JSONObject;

import com.savagelook.android.UrlJsonAsyncTask;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class HistoricalSales extends Activity {
	
	private static final String DAYS_URL = Global.getInstance().getDevelopment() + "/api/v1/days.json";
	private static final String WEEKS_URL = Global.getInstance().getDevelopment() + "/api/v1/weeks.json";
	private static final String MONTHS_URL = Global.getInstance().getDevelopment() + "/api/v1/months.json";
	
	private SharedPreferences mPreferences;
	
	EditText days, weeks, months;
	TextView p1, p2, p3, reportDescriptionText;
	TableRow reportDescription, historicalTitles;
	Button goBack, historicalDialog;
	
	Typeface tf;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_historical_sales);
		
		tf = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueDeskUI.ttf");
		
		mPreferences = getSharedPreferences("CurrentUser", MODE_PRIVATE);
		
		historicalTitles = (TableRow)findViewById(R.id.historicalTitles);
		historicalTitles.setVisibility(View.GONE);
		
		reportDescriptionText = (TextView) findViewById(R.id.reportDescriptionText);
		reportDescriptionText.setTypeface(tf);
		
		goBack = (Button) findViewById(R.id.goBack);
		goBack.setTypeface(tf);
		historicalDialog = (Button) findViewById(R.id.historicalDialog);
		historicalDialog.setTypeface(tf);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.historical_sales, menu);
		return true;
	}

	public void showHistoricalDialog(View view) {

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.historical_dialog);
    	
		TextView daysLabel = (TextView) dialog.findViewById(R.id.historicalDaysLabel);
		TextView weeksLabel = (TextView) dialog.findViewById(R.id.historicalWeeksLabel);
		TextView monthsLabel = (TextView) dialog.findViewById(R.id.historicalMonthsLabel);
		
		daysLabel.setTypeface(tf);
		weeksLabel.setTypeface(tf);
		monthsLabel.setTypeface(tf);
		
		EditText daysET = (EditText) dialog.findViewById(R.id.historicalDays);
		EditText weeksET = (EditText) dialog.findViewById(R.id.historicalWeeks);
		EditText monthsET = (EditText) dialog.findViewById(R.id.historicalMonths);
		
		daysET.setTypeface(tf);
		weeksET.setTypeface(tf);
		monthsET.setTypeface(tf);
		
		// Evento para capturar los botones
		Button daysBtn = (Button) dialog.findViewById(R.id.generateDays);
		daysBtn.setTypeface(tf);
		daysBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				days = (EditText)dialog.findViewById(R.id.historicalDays);
				days.setTypeface(tf);
				historicalDays(Integer.parseInt(days.getText().toString()));
			}
		});
    	
		// Evento para capturar los botones
		Button weeksBtn = (Button) dialog.findViewById(R.id.generateWeeks);
		weeksBtn.setTypeface(tf);
		weeksBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				weeks = (EditText)dialog.findViewById(R.id.historicalWeeks);
				weeks.setTypeface(tf);
				historicalWeeks(Integer.parseInt(weeks.getText().toString()));
			}
		});
    	
		// Evento para capturar los botones
		Button monthsBtn = (Button) dialog.findViewById(R.id.generateMonths);
		monthsBtn.setTypeface(tf);
		monthsBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				months = (EditText)dialog.findViewById(R.id.historicalMonths);
				months.setTypeface(tf);
				historicalMonths(Integer.parseInt(months.getText().toString()));
			}
		});
		
		dialog.show();
	}
	
	public void historicalDays(int days) {
		if(Global.getInstance().hasConnection(HistoricalSales.this)){
			GetDays getDays = new GetDays(HistoricalSales.this);
			getDays.setMessageLoading("Cargando...");
			getDays.execute(DAYS_URL + "?authentication_token=" + mPreferences.getString("AuthToken", "") + "&days=" + days);
		}else{
			Toast.makeText(HistoricalSales.this, "El dispositivo no tiene conexi�n a Internet.", Toast.LENGTH_LONG).show();
		}
	}
	
	public void historicalWeeks(int weeks) {
		if(Global.getInstance().hasConnection(HistoricalSales.this)){
			GetWeeks getWeeks = new GetWeeks(HistoricalSales.this);
			getWeeks.setMessageLoading("Cargando...");
			getWeeks.execute(WEEKS_URL + "?authentication_token=" + mPreferences.getString("AuthToken", "") + "&weeks=" + weeks);
		}else{
			Toast.makeText(HistoricalSales.this, "El dispositivo no tiene conexi�n a Internet.", Toast.LENGTH_LONG).show();
		}
	}
	
	public void historicalMonths(int months) {
		if(Global.getInstance().hasConnection(HistoricalSales.this)){
			GetMonths getMonths = new GetMonths(HistoricalSales.this);
			getMonths.setMessageLoading("Cargando...");
			getMonths.execute(MONTHS_URL + "?authentication_token=" + mPreferences.getString("AuthToken", "") + "&months=" + months);
		}else{
			Toast.makeText(HistoricalSales.this, "El dispositivo no tiene conexi�n a Internet.", Toast.LENGTH_LONG).show();
		}
	}
	
	private class GetDays extends UrlJsonAsyncTask {
	    public GetDays(Context context) {
	        super(context);
	    }

		@Override
        protected void onPostExecute(JSONObject json) {
            try {
            	
            	reportDescription = (TableRow)findViewById(R.id.reportDescription);
            	reportDescription.setVisibility(View.GONE);
        		historicalTitles.setVisibility(View.VISIBLE);
            	
                JSONArray jsonParameters = json.getJSONArray("parameters");
                JSONArray jsonData = json.getJSONArray("data");
                JSONArray jsonTotals = json.getJSONArray("totals");
                int size = jsonData.length();
                
                TableLayout historicalLayout = (TableLayout) findViewById(R.id.historicalTable);
                
                for(int n = 2, s = historicalLayout.getChildCount(); n < s; ++n) {
                	TableRow row = (TableRow)historicalLayout.getChildAt(n);
                	row.setVisibility(View.GONE);
                }
                
                p1 = (TextView)findViewById(R.id.p1name);
                p2 = (TextView)findViewById(R.id.p2name);
                p3 = (TextView)findViewById(R.id.p3name);
                
                p1.setText(jsonParameters.getJSONObject(0).getString("n1"));
                p2.setText(jsonParameters.getJSONObject(0).getString("n2"));
                p3.setText(jsonParameters.getJSONObject(0).getString("n3"));
                
                p1.setTypeface(tf);
                p2.setTypeface(tf);
                p3.setTypeface(tf);
                
                for(int i = 0; i < size; ++i){
                	TableRow row = (TableRow) getLayoutInflater().inflate(R.layout.historical_row, null);
            		((TextView)row.findViewById(R.id.dayName)).setText(jsonData.getJSONObject(i).getString("name"));
            		((TextView)row.findViewById(R.id.p1)).setText(jsonData.getJSONObject(i).getString("t1"));
            		((TextView)row.findViewById(R.id.p2)).setText(jsonData.getJSONObject(i).getString("t2"));
            		((TextView)row.findViewById(R.id.p3)).setText(jsonData.getJSONObject(i).getString("t3"));
            		((TextView)row.findViewById(R.id.dayName)).setTypeface(tf);
            		((TextView)row.findViewById(R.id.p1)).setTypeface(tf);
            		((TextView)row.findViewById(R.id.p2)).setTypeface(tf);
            		((TextView)row.findViewById(R.id.p3)).setTypeface(tf);
            		historicalLayout.addView(row);
            		historicalLayout.requestLayout();
                }
                
                TableRow row = (TableRow) getLayoutInflater().inflate(R.layout.historical_row, null);
        		((TextView)row.findViewById(R.id.dayName)).setText("Total");
        		((TextView)row.findViewById(R.id.p1)).setText(jsonTotals.getJSONObject(0).getString("total1"));
        		((TextView)row.findViewById(R.id.p2)).setText(jsonTotals.getJSONObject(0).getString("total2"));
        		((TextView)row.findViewById(R.id.p3)).setText(jsonTotals.getJSONObject(0).getString("total3"));
        		((TextView)row.findViewById(R.id.dayName)).setTypeface(tf);
        		((TextView)row.findViewById(R.id.p1)).setTypeface(tf);
        		((TextView)row.findViewById(R.id.p2)).setTypeface(tf);
        		((TextView)row.findViewById(R.id.p3)).setTypeface(tf);
        		historicalLayout.addView(row);
        		historicalLayout.requestLayout();
                
            } catch (Exception e) {
            	Toast.makeText(context, "Ocurri� un error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            } finally {
            	super.onPostExecute(json);
            }
	    }
	}
	
	private class GetWeeks extends UrlJsonAsyncTask {
	    public GetWeeks(Context context) {
	        super(context);
	    }

		@Override
        protected void onPostExecute(JSONObject json) {
            try {
            	
            	reportDescription = (TableRow)findViewById(R.id.reportDescription);
            	reportDescription.setVisibility(View.GONE);
        		historicalTitles.setVisibility(View.VISIBLE);
            	
                JSONArray jsonParameters = json.getJSONArray("parameters");
                JSONArray jsonData = json.getJSONArray("data");
                JSONArray jsonTotals = json.getJSONArray("totals");
                int size = jsonData.length();
                
                TableLayout historicalLayout = (TableLayout) findViewById(R.id.historicalTable);
                
                for(int n = 2, s = historicalLayout.getChildCount(); n < s; ++n) {
                	TableRow row = (TableRow)historicalLayout.getChildAt(n);
                	row.setVisibility(View.GONE);
                }
                
                p1 = (TextView)findViewById(R.id.p1name);
                p2 = (TextView)findViewById(R.id.p2name);
                p3 = (TextView)findViewById(R.id.p3name);
                
                p1.setText(jsonParameters.getJSONObject(0).getString("n1"));
                p2.setText(jsonParameters.getJSONObject(0).getString("n2"));
                p3.setText(jsonParameters.getJSONObject(0).getString("n3"));
                
                p1.setTypeface(tf);
                p2.setTypeface(tf);
                p3.setTypeface(tf);
                
                for(int i = 0; i < size; ++i){
                	TableRow row = (TableRow) getLayoutInflater().inflate(R.layout.historical_row, null);
            		((TextView)row.findViewById(R.id.dayName)).setText(jsonData.getJSONObject(i).getString("name"));
            		((TextView)row.findViewById(R.id.p1)).setText(jsonData.getJSONObject(i).getString("t1"));
            		((TextView)row.findViewById(R.id.p2)).setText(jsonData.getJSONObject(i).getString("t2"));
            		((TextView)row.findViewById(R.id.p3)).setText(jsonData.getJSONObject(i).getString("t3"));
            		((TextView)row.findViewById(R.id.dayName)).setTypeface(tf);
            		((TextView)row.findViewById(R.id.p1)).setTypeface(tf);
            		((TextView)row.findViewById(R.id.p2)).setTypeface(tf);
            		((TextView)row.findViewById(R.id.p3)).setTypeface(tf);
            		historicalLayout.addView(row);
            		historicalLayout.requestLayout();
                }
                
                TableRow row = (TableRow) getLayoutInflater().inflate(R.layout.historical_row, null);
        		((TextView)row.findViewById(R.id.dayName)).setText("Total");
        		((TextView)row.findViewById(R.id.p1)).setText(jsonTotals.getJSONObject(0).getString("total1"));
        		((TextView)row.findViewById(R.id.p2)).setText(jsonTotals.getJSONObject(0).getString("total2"));
        		((TextView)row.findViewById(R.id.p3)).setText(jsonTotals.getJSONObject(0).getString("total3"));
        		((TextView)row.findViewById(R.id.dayName)).setTypeface(tf);
        		((TextView)row.findViewById(R.id.p1)).setTypeface(tf);
        		((TextView)row.findViewById(R.id.p2)).setTypeface(tf);
        		((TextView)row.findViewById(R.id.p3)).setTypeface(tf);
        		historicalLayout.addView(row);
        		historicalLayout.requestLayout();
                
            } catch (Exception e) {
            	Toast.makeText(context, "Ocurri� un error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            } finally {
            	super.onPostExecute(json);
            }
	    }
	}
	
	private class GetMonths extends UrlJsonAsyncTask {
	    public GetMonths(Context context) {
	        super(context);
	    }

		@Override
        protected void onPostExecute(JSONObject json) {
            try {
            	
            	reportDescription = (TableRow)findViewById(R.id.reportDescription);
            	reportDescription.setVisibility(View.GONE);
        		historicalTitles.setVisibility(View.VISIBLE);
            	
                JSONArray jsonParameters = json.getJSONArray("parameters");
                JSONArray jsonData = json.getJSONArray("data");
                JSONArray jsonTotals = json.getJSONArray("totals");
                int size = jsonData.length();
                
                TableLayout historicalLayout = (TableLayout) findViewById(R.id.historicalTable);
                
                for(int n = 2, s = historicalLayout.getChildCount(); n < s; ++n) {
                	TableRow row = (TableRow)historicalLayout.getChildAt(n);
                	row.setVisibility(View.GONE);
                }
                
                p1 = (TextView)findViewById(R.id.p1name);
                p2 = (TextView)findViewById(R.id.p2name);
                p3 = (TextView)findViewById(R.id.p3name);
                
                p1.setText(jsonParameters.getJSONObject(0).getString("n1"));
                p2.setText(jsonParameters.getJSONObject(0).getString("n2"));
                p3.setText(jsonParameters.getJSONObject(0).getString("n3"));
                
                p1.setTypeface(tf);
                p2.setTypeface(tf);
                p3.setTypeface(tf);
                
                for(int i = 0; i < size; ++i){
                	TableRow row = (TableRow) getLayoutInflater().inflate(R.layout.historical_row, null);
            		((TextView)row.findViewById(R.id.dayName)).setText(jsonData.getJSONObject(i).getString("name"));
            		((TextView)row.findViewById(R.id.p1)).setText(jsonData.getJSONObject(i).getString("t1"));
            		((TextView)row.findViewById(R.id.p2)).setText(jsonData.getJSONObject(i).getString("t2"));
            		((TextView)row.findViewById(R.id.p3)).setText(jsonData.getJSONObject(i).getString("t3"));
            		((TextView)row.findViewById(R.id.dayName)).setTypeface(tf);
            		((TextView)row.findViewById(R.id.p1)).setTypeface(tf);
            		((TextView)row.findViewById(R.id.p2)).setTypeface(tf);
            		((TextView)row.findViewById(R.id.p3)).setTypeface(tf);
            		historicalLayout.addView(row);
            		historicalLayout.requestLayout();
                }
                
                TableRow row = (TableRow) getLayoutInflater().inflate(R.layout.historical_row, null);
        		((TextView)row.findViewById(R.id.dayName)).setText("Total");
        		((TextView)row.findViewById(R.id.p1)).setText(jsonTotals.getJSONObject(0).getString("total1"));
        		((TextView)row.findViewById(R.id.p2)).setText(jsonTotals.getJSONObject(0).getString("total2"));
        		((TextView)row.findViewById(R.id.p3)).setText(jsonTotals.getJSONObject(0).getString("total3"));
        		((TextView)row.findViewById(R.id.dayName)).setTypeface(tf);
        		((TextView)row.findViewById(R.id.p1)).setTypeface(tf);
        		((TextView)row.findViewById(R.id.p2)).setTypeface(tf);
        		((TextView)row.findViewById(R.id.p3)).setTypeface(tf);
        		historicalLayout.addView(row);
        		historicalLayout.requestLayout();
                
            } catch (Exception e) {
            	Toast.makeText(context, "Ocurri� un error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            } finally {
            	super.onPostExecute(json);
            }
	    }
	}
	
	public void goBack(View view){
        Intent intent = new Intent(getApplicationContext(), POSActivity.class);
        startActivity(intent);
        finish();
	}
	
}
