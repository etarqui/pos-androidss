package com.sietecumbres.pos;

public class Request {

	// private variables
	public int _id;
	public int _total_price;

	public Request() {
	}

	// constructor
	public Request(int id, int total_price) {
		this._id = id;
		this._total_price = total_price;

	}

	// constructor
	public Request(int total_price) {
		this._total_price = total_price;
	}

	// getting ID
	public int getID() {
		return this._id;
	}

	// setting id
	public void setID(int id) {
		this._id = id;
	}

	// getting name
	public int getTotalPrice() {
		return this._total_price;
	}

	// setting name
	public void setTotalPrice(int total_price) {
		this._total_price = total_price;
	}

}