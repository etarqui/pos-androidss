package com.sietecumbres.pos;

public class ModelCones {

	private String productId;
	private String productName;
	private String productImage;
	private String productFixedPrice;
	private int productPrice;
	private int minWeight;
	private int conePrice;
	private int maxWeight;

	public ModelCones(String productId, String productName, String productImage, String productFixedPrice, int productPrice, int minWeight, int conePrice, int maxWeight)
	{
		this.productId  = productId;
		this.productName  = productName;
		this.productImage  = productImage;
		this.productFixedPrice  = productFixedPrice;
		this.productPrice = productPrice;
		this.minWeight = minWeight;
		this.conePrice = conePrice;
		this.setMaxWeight(maxWeight);
	}

	public String getProductId() {
		return productId;
	}

	public String getProductName() {
		return productName;
	}
	
	public void setProductName(String name){
		this.productName = name;
	}

	public String getProductImage() {
		return productImage;
	}

	public String getProductFixedPrice() {
		return productFixedPrice;
	}

	public int getProductPrice() {
		return productPrice;
	}
	
	public void setProductPrice(int price){
		this.productPrice = price;
	}

	public int getMinWeight() {
		return minWeight;
	}

	public void setMinWeight(int minWeight) {
		this.minWeight = minWeight;
	}

	public int getConePrice() {
		return conePrice;
	}

	public void setConePrice(int conePrice) {
		this.conePrice = conePrice;
	}

	public int getMaxWeight() {
		return maxWeight;
	}

	public void setMaxWeight(int maxWeight) {
		this.maxWeight = maxWeight;
	}

}